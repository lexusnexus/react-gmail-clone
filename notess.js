 <div>
		            		    <hr/>
		            		      <h3>ID: {specificData[0].id}</h3>
		            		      <hr/>
		            		      {/*let car = cars.find(car => car.color === "red");*/}
		            		      <h3>To: {specificData[0].payload.headers[5].value}</h3>
		            		      <h3>From: {specificData[0].payload.headers[4].value}</h3>
		            		      <h3>Subject: {specificData[0].payload.headers[3].value}</h3>
		            		      <hr/>
		            		      <h3>Body: {atob(specificData[0].payload.parts[0].body.data)}</h3>

		            		       <hr/>
		            		      <h3>ID: {specificData[1].id}</h3>
		            		      <hr/>
		            		      {/*let car = cars.find(car => car.color === "red");*/}
		            		      <h3>To: {specificData[1].payload.headers[5].value}</h3>
		            		      <h3>From: {specificData[1].payload.headers[4].value}</h3>
		            		      <h3>Subject: {specificData[1].payload.headers[3].value}</h3>
		            		      <hr/>
		            		      <h3>Body: {atob(specificData[1].payload.parts[0].body.data)}</h3>

		            		       <hr/>
		            		      <h3>ID: {specificData[2].id}</h3>
		            		      <hr/>
		            		      {/*let car = cars.find(car => car.color === "red");*/}
		            		      <h3>To: {specificData[2].payload.headers[5].value}</h3>
		            		      <h3>From: {specificData[2].payload.headers[4].value}</h3>
		            		      <h3>Subject: {specificData[2].payload.headers[3].value}</h3>
		            		      <hr/>
		            		      <h3>Body: {atob(specificData[2].payload.parts[0].body.data)}</h3>
		            		    </div>

		            		    

		            		    const array1 = [{id: '17fc57443105a574', threadId: '17fc57443105a574'},{id: '17fc4f934c2583a9', threadId: '17fc4f934c2583a9'}
,{id: '17fc4d7b97127098', threadId: '17fc4d7b97127098'}];

// pass a function to map
const map1 = array1.map(x => ({id : x.id,date : x.id.slice(0, -5)}));

console.log(map1);
// expected output: Array [2, 8, 18, 32]
function cleanTimestamp(ts) {
    if (!ts) {
        return "";
    }
    ts = ts.replace(/[`'"\s\,]+/g, '');
    if (ts.charAt(ts.length - 1) == "L") {
        ts = ts.slice(0, -1);
    }
    return ts;
}

function EpochToHuman() {
    var iorg = $('#ecinput').val();
    var hr = "<br/>&nbsp;";
    var errormessage = "Sorry, this timestamp is not valid.<br/>Check your timestamp, strip letters and punctuation marks.";
    var outputtext = "";
    var notices = "";
    inputtext = cleanTimestamp(iorg);
    if (inputtext && inputtext != iorg.trim()) {
        outputtext += "Converting " + inputtext + ":<br/>";
    }
    if ((inputtext.length === 0) || isNaN(inputtext)) {
        if (isHex(inputtext)) {
            inputtext = '0x' + inputtext;
        } else {
            $("#result1").html(errormessage + hr);
            return;
        }
    }
    if (inputtext.substring(0, 2) == '0x') {
        outputtext += "Converting <a href=\"/hex?q=" + inputtext.substring(2) + "\">hexadecimal timestamp</a> to decimal: " + parseInt(inputtext) + "<br/>";
    }
    inputtext = inputtext * 1;
    if (!Ax()) inputtext -= inputtext;
    var epoch = inputtext;
    var cn = '';
    if (locale.substring(0, 2) == 'en') {
        cn = ' class="utcal"';
    }
    if ((inputtext >= 10E7) && (inputtext < 18E7)) {
        notices += '<br/>Expected a more recent date? You are missing 1 digit.';
    }
    if ((inputtext >= 1E16) || (inputtext <= -1E16)) {
        outputtext += "Assuming that this timestamp is in <b>nanoseconds (1 billionth of a second)</b>:<br/>";
        inputtext = Math.floor(inputtext / 1000000);
    } else if ((inputtext >= 1E14) || (inputtext <= -1E14)) {
        outputtext += "Assuming that this timestamp is in <b>microseconds (1/1,000,000 second)</b>:<br/>";
        inputtext = Math.floor(inputtext / 1000);
    } else if ((inputtext >= 1E11) || (inputtext <= -3E10)) {
        outputtext += "Assuming that this timestamp is in <b>milliseconds</b>:<br/>";
    } else {
        outputtext += "Assuming that this timestamp is in <b>seconds</b>:<br/>";
        if ((inputtext > 1E11) || (inputtext < -1E10)) {
            notices += "<br>Remove the last 3 digits if you are trying to convert milliseconds.";
        }
        inputtext = (inputtext * 1000);
    }
    if (inputtext < -68572224E5) {
        notices += "<br/>Dates before 14 september 1752 (pre-Gregorian calendar) are not accurate.";
    }
    var datum = new Date(inputtext);
    if (isValidDate(datum)) {
        var convertedDate = datum.epochConverterGMTString();
        var relativeDate = datum.relativeDate();
        outputtext += "<b" + cn + ">GMT</b>: " + convertedDate;
        outputtext += "<br/><b" + cn + ">Your time zone</b>: <span title=\"" + datum.toDateString() + " " + datum.toTimeString() + "\">" + datum.epochConverterLocaleString(1) + "</span>";
        if (typeof moment !== "undefined") {
            outputtext += " <a title=\"convert to other time zones\" href=\"https://www.epochconverter.com/timezones?q=" + epoch + "\">" + datum.printLocalTimezone() + "</a>";
            var md = moment(datum);
            if (md.isDST()) {
                outputtext += ' <span class="help" title="daylight saving/summer time">DST</span>';
                if (datum.getFullYear() < 1908) notices += '<br/>DST (Daylight Saving Time) was first used around 1908.<br/>Your browser uses the current DST rules for all dates in history.';
            }
        }
        if (relativeDate) {
            outputtext += "<br/><b" + cn + ">Relative</b>: " + relativeDate.capitalize();
        }
        if (notices) outputtext += "<br/><br/>Note: " + notices;
    } else {
        outputtext += errormessage;
    }
    $("#result1").html(outputtext + hr);
}

const array1 = [1, 4, 9, 16];

// pass a function to map
const map1 = array1.map(x => x * 2);

console.log(map1);
// expected output: Array [2, 8, 18, 32]
inputtext = "0x17fc4f934c2";
inputtext = parseInt(inputtext);
var datum = new Date(inputtext);
console.log(datum)
console.log(parseInt(inputtext));
console.log(datum.toDateString()+" "+datum.toTimeString())\



 const array1 = [{id: '17fc57443105a574', threadId: '17fc57443105a574'},{id: '17fc4f934c2583a9', threadId: '17fc4f934c2583a9'}
,{id: '17fc4d7b97127098', threadId: '17fc4d7b97127098'}];

// pass a function to map
const map1 = array1.map(x => ({id : x.id,date : new Date(
	parseInt("0x"+x.id.slice(0, -5)).toDateString()+" "+new Date(parseInt("0x"+x.id.slice(0, -5)).toTimeString()))}));

console.log(map1);
// expected output: Array [2, 8, 18, 32]
inputtext = "0x17fc4f934c2";
inputtext = parseInt(inputtext);
var datum = new Date(parseInt("0x"+x.id.slice(0, -5)).toDateString()+" "+new Date(parseInt("0x"+x.id.slice(0, -5)).toTimeString());

console.log(datum.toUTCString()

	inputtext = "0x17fc4f934c2";
inputtext = parseInt(inputtext);
var datum = new Date(inputtext);

console.log(datum.toLocaleString('en-US', { hour12: false }))

new Date(parseInt("0x"+x.id.slice(0, -5))).toLocaleString('en-US', { hour12: false })

 const array1 = [{id: '17fc57443105a574', threadId: '17fc57443105a574'},{id: '17fc4f934c2583a9', threadId: '17fc4f934c2583a9'}
,{id: '17fc4d7b97127098', threadId: '17fc4d7b97127098'}];

// pass a function to map
const map1 = array1.map(x => ({id : x.id,date :new Date(parseInt("0x"+x.id.slice(0, -5))).toLocaleString('en-US', { hour12: false })}));

console.log(map1);

inputtext = "0x17fc4f934c2";
inputtext = parseInt(inputtext);
var datum = new Date(inputtext);

console.log(datum.toLocaleString('en-US', { hour12: false }))


console.log(event.toLocaleDateString('en-US', { day: 'numeric',month: 'short' }).split(' ').reverse().join(' '));
// 25 Mar
